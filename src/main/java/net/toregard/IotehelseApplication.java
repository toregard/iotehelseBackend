package net.toregard;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IotehelseApplication {

	public static void main(String[] args) {
		SpringApplication.run(IotehelseApplication.class, args);
	}
}
