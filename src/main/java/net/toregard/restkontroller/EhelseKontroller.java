package net.toregard.restkontroller;

import net.toregard.entities.Hendelser;
import net.toregard.persistence.HendelserRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/ehelse")
public class EhelseKontroller {
    @Autowired
    private HendelserRepository hendelserRepository;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    @RequestMapping(
            value= "/melding/{mobilId}/{knappTrykket}",
            produces = "application/json",
            method = RequestMethod.GET)
    public boolean mottaData(
            @PathVariable String mobilId,
            @PathVariable String knappTrykket) {
        boolean mottatt=false;
              log.info(String.format("Cloud mottatt data: %s %s",mobilId,knappTrykket));
        Hendelser hendelser =new Hendelser();
        hendelser.setDato(LocalDateTime.now());
        hendelser.setMobilId(mobilId);
        hendelser.setTypeHendelse(knappTrykket);
        hendelserRepository.saveAndFlush(hendelser);
              //              Hendelser hendelser =
//                      Hendelser.builder().dato(LocalDateTime.now()).
//                                     mobilId(mobilId).
//                            typeHendelse(knappTrykket).build();
//              hendelserRepository.saveAndFlush(hendelser);
              return  true;
    }

    @RequestMapping(
            value= "/melding/hentalleasc",
            produces = "application/json",
            method = RequestMethod.GET)
    public List<Hendelser> getAllSortedByDateAsc(){
        log.info("findAllByDateAsc:");
        return hendelserRepository.findAll(new Sort(Sort.Direction.ASC, "dato"));
    }
}
