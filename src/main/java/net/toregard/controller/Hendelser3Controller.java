package net.toregard.controller;

import net.toregard.persistence.HendelserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class Hendelser3Controller {
    @Autowired
    private HendelserRepository hendelserRepository;

    @RequestMapping("/hendelser3")
    public String hendelser3() {
        return "index";
    }

}
