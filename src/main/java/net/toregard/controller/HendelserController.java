package net.toregard.controller;

import net.toregard.entities.Hendelser;
import net.toregard.persistence.HendelserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
public class HendelserController {
    @Autowired
    private HendelserRepository hendelserRepository;

    @RequestMapping("/hendelser")
    public String hendlelser(@RequestParam(value="mobileId", required=false, defaultValue="") String mobileId, Model model) {
        model.addAttribute("mobileId", mobileId);
        List<Hendelser> list =hendelserRepository.findAll(new Sort(Sort.Direction.DESC, "dato"));
        model.addAttribute("hendelser", list);
        return "hendelser";
    }

}
