package net.toregard.persistence;

import net.toregard.entities.Hendelser;
import org.springframework.context.annotation.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HendelserRepository extends JpaRepository<Hendelser, Long> {



}