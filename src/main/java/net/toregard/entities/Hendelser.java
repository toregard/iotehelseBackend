package net.toregard.entities;

import com.fasterxml.jackson.annotation.JsonFormat;
import groovy.transform.builder.Builder;
import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import java.io.Serializable;
import java.time.LocalDateTime;

@Entity
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class Hendelser implements Serializable
    {
        private static final long serialVersionUID = -3172329135144019188L;
        @Id
        @GeneratedValue(strategy = GenerationType.AUTO)
        private Long id;
        @JsonFormat(pattern="yyyy-MM-dd mm:ss")
        @DateTimeFormat(iso = DateTimeFormat.ISO.TIME)
        private LocalDateTime dato;
        private String mobilId;
        private String typeHendelse;
    }
